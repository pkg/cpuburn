# Danish translation cpuburn.
# Copyright (C) 2010 cpuburn & nedenstående oversættere.
# This file is distributed under the same license as the cpuburn package.
# Claus Hindsgaul <claus_h@image.dk>, 2004.
# Joe Hansen <joedalton2@yahoo.dk>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: cpuburn\n"
"Report-Msgid-Bugs-To: cpuburn@packages.debian.org\n"
"POT-Creation-Date: 2008-03-20 18:42+0100\n"
"PO-Revision-Date: 2010-06-17 17:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org> \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. Type: note
#. Description
#: ../cpuburn.templates:1001
msgid "cpuburn is dangerous for your system"
msgstr "cpuburn er farligt for dit system"

#. Type: note
#. Description
#: ../cpuburn.templates:1001
msgid ""
"This program is designed to heavily load CPU chips. Undercooled, overclocked "
"or otherwise weak systems may fail causing data loss (filesystem corruption) "
"and possibly permanent damage to electronic components. Use at your own risk."
msgstr ""
"Dette program er lavet til at sætte CPU-chips under stor belastning. Dårligt "
"afkølede, overclockede eller på anden måde svage systemer kan fejle og "
"dermed føre til datatab (rod i filsystemet) og muligvis permanent skade de "
"elektroniske komponenter. Brug det på eget ansvar."

#. Type: note
#. Description
#: ../cpuburn.templates:1001
msgid "For more information, see /usr/share/doc/cpuburn/README."
msgstr "For flere oplysninger, se /usr/share/doc/cpuburn/README."

#. Type: text
#. Description
#: ../cpuburn-udeb.templates:1001
msgid "Perform CPU stress test (burn in)"
msgstr "Udfør testen CPU-stres (burn in)"

#. Type: boolean
#. Description
#: ../cpuburn-udeb.templates:2001
msgid "Are you sure you want to perform a burn test?"
msgstr "Er du sikker på, at du ønsker at udføre en brændingstest?"

#. Type: boolean
#. Description
#: ../cpuburn-udeb.templates:2001
msgid ""
"This process is designed to heavily load CPU chips. Undercooled, overclocked "
"or otherwise weak systems may fail causing possibly permanent damage to "
"electronic components. Use at your own risk."
msgstr ""
"Denne proces er lavet til at sætte CPU-chips under stor belastning. Dårligt "
"afkølede, overclockede eller på anden måde svage systemer kan fejle og "
"muligvis medføre permanent skade på de elektroniske komponenter. Brug det på "
"eget ansvar."

#. Type: boolean
#. Description
#: ../cpuburn-udeb.templates:2001
msgid "Users should read the cpuburn documentation carefully before use."
msgstr "Brugere skal omhyggeligt læse cpuburndokumentationen før brug."

#. Type: select
#. Choices
#: ../cpuburn-udeb.templates:3001
msgid "10m"
msgstr "10 m"

#. Type: select
#. Choices
#: ../cpuburn-udeb.templates:3001
msgid "30m"
msgstr "30 m"

#. Type: select
#. Choices
#: ../cpuburn-udeb.templates:3001
msgid "1h"
msgstr "1 t"

#. Type: select
#. Choices
#: ../cpuburn-udeb.templates:3001
msgid "6h"
msgstr "6 t"

#. Type: select
#. Choices
#: ../cpuburn-udeb.templates:3001
msgid "24h"
msgstr "24 t"

#. Type: select
#. Description
#: ../cpuburn-udeb.templates:3002
msgid "Burn-in duration:"
msgstr "Varighed af brænding:"

#. Type: select
#. Description
#: ../cpuburn-udeb.templates:3002
msgid "Please select the duration of the CPU stress test."
msgstr "Vælg venligst varighed på testen for CPU-stres."

#. Type: text
#. Description
#: ../cpuburn-udeb.templates:4001
msgid "Performing hardware burn-in"
msgstr "Udfører hardwarebrænding (burn-in)"

#. Type: text
#. Description
#. Translators, ${PROGRESS} corresponds to a small animation to show
#. that the machine is still alive.
#: ../cpuburn-udeb.templates:5001
msgid "Performing hardware burn-in. This takes some time... ${PROGRESS}"
msgstr "Udfører hardwarebrænding (burn-in). Dette tager lidt tid... ${PROGRESS}"


